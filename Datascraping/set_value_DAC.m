function set_value_DAC(dev,n, dataIn)
    % dev device to write in
    % n between 1 and 9 correspond to DAC registers A to H and All DACs
    % dataIn value integer between 0 and 4096 (12 bits) to assign to DAC
    switch n
        case 1
%             register= bin2dec('00110000');
            register= bin2dec('00110011'); % case 4
        case 2
%             register= bin2dec('00110001');
            register= bin2dec('00110010'); % case 3
        case 3
%             register= bin2dec('00110010');
             register= bin2dec('00110001'); % case 2
        case 4
%             register= bin2dec('00110011');  
              register= bin2dec('00110000'); % case 1
        case 5
%             register= bin2dec('00110100');  
              register= bin2dec('00110111'); % case 8
        case 6
%             register= bin2dec('00110101');  
              register= bin2dec('00110110'); % case 7
        case 7
%             register= bin2dec('00110110');  
              register= bin2dec('00110101'); % case 6
        case 8
%             register= bin2dec('00110111');  
              register= bin2dec('00110100'); % case 5
        case 9
            register= bin2dec('00111111');  
        otherwise
            disp('Choose n between 1 and 9');      
    end
    dataIn=dataIn*2^4; % Conversion 12 to 16 bits for the writing: The 4 last bits are unused 
    writeRegister(dev,register,dataIn,'uint16');
end