
function B = newImage(dev,fid,fileName, frameNum)
% Acquire the first image, .ptw file to read with a header
% Definition of image parameters
for ii=frameNum
    
    LgthFileMainHeader = 3476;
    LgthImHeader = 1016;
    
    tic
    Nbimage=[];
    while isempty(Nbimage)==1
        trigger(dev)
        %Seek the total number of image:
        fseek(fid, 27, 'bof');
        Nbimage = fread(fid,1,'uint32');
    end
    
    
    %Seek the number pixel in an image:
    fseek(fid, 23, 'bof');
    NbPixelImage = fread(fid,1,'uint32');
    %Seek the total number of image:
    fseek(fid, 27, 'bof');
    Nbimage = fread(fid,1,'uint32');
    %Seek the height of the images
    fseek(fid, 377, 'bof');
    NbColImage = fread(fid,1,'uint16');
    %Seek the width of the images
    fseek(fid, 379, 'bof');
    NbRowImage = fread(fid,1,'uint16');
    
    %Set the file point on the header of the first image
    fseek(fid, LgthFileMainHeader, 'bof');
    
    h = waitbar(0,[fileName ' database importation : ' num2str(0) '/' num2str(Nbimage) ]);
    
    h=waitbar(ii/Nbimage,h,[fileName ' database importation : ' num2str(ii) '/' num2str(Nbimage) ]);
    
    
    Image=0;
    while Image~=2*ii
        trigger(dev)
        
        %Seek for the last image
        fseek(fid, LgthFileMainHeader, 'bof');
        fread(fid,LgthImHeader);
        A=fread( fid , NbPixelImage , 'uint16' );
        Image=0;
        while size(A)==[NbPixelImage,1]
            Image=Image+1;
            fread(fid,LgthImHeader);
            A=[];
            A=fread( fid , NbPixelImage , 'uint16' );
            if size(A)==[NbPixelImage,1]
            end
        end
    end
    
    % Seek for the last image
    fseek(fid, LgthFileMainHeader, 'bof');
    for kk=1:2*ii-1
        fread(fid,LgthImHeader);
        fread( fid , NbPixelImage , 'uint16' );
    end
    
    %     read image
    fread(fid,LgthImHeader);
    %Jump the image header
    B = fread( fid , [NbColImage,NbRowImage] , 'uint16' );
    
    % Normalise B Matrix
    B = B./max(B, [], 'all');
    
%     threshold = 0.45;
%     
%     
%     for rows = 1:640
%         for cols = 1:512
%             if (B(rows,cols) < threshold)
%                 B(rows,cols) = 0;
%             end
%             %             plotDenom = plot(startMatrixCoordXX+i, endMatrixCoordYY+j, 'ro');
%         end
%     end
    
    %
    
    % Display image
    figure(frameNum)
    imagesc(B,[0 1])
%      xlim([170, 340]);
%     ylim([200, 400]);
    c=colorbar('Limits', [0 1]);
    
    toc
end