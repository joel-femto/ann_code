% Function to transmit a voltage (and therefore current) to DAC from
% Arduino 
% Note: Current value is in order of mA

function sendCurrentArduino(curControlDev, currentVector, channelNum)
    
    % 100 mA ==> 1 V 
    % Therefore 1 mA ==> 1/100 V
    
    % 1 V ==> 3277 (12-bits)
    % Assume R = 2000;
    
    % Therefore 3277/100 ==> 1mA
    fprintf("currentVector length: %d", length(currentVector));
    fprintf("channelNum length: %d", length(channelNum));

    
    if length(currentVector) ~= length(channelNum)
        disp("Error: The length of currentVector should be the same as the length of channelNum");
        return
    end
    
%     resistance = 2000;
    
    for i = 1:length(currentVector)
        %% OutputValue = round(current(mA) * 3277/100(mA)) ==> current in [0,100mA]
        outputValue = round(currentVector(i) * 3277/100);
        %3277; ==> 1 V = 100mA current ==> voltage(i) = (currentVector(i) * 3277/100);
        fprintf('CurrentVector = %f',currentVector(i));
        fprintf('Sending %d mA --> DAC register %d.\n', outputValue*(100/3277), channelNum(i));
        set_value_DAC(curControlDev, channelNum(i), outputValue);
    end
           

end