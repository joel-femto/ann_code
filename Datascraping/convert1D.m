% Function which takes in 2-D Image (512 x 640 or 640 x 512) and transforms
% to 1-D signal along specified lateral and longitudinal ranges 

function outputSignal = convert1D(matrixImg, startLateral, endLateral, ...
    startLongitude, endLongitude)

matrixImg = rot90(matrixImg);

% Splice matrix to region of interest 
newImage = matrixImg(startLongitude:endLongitude,startLateral:endLateral);

figure(); imagesc(newImage)

% Sum every column and normalise result
sumRows = sum(newImage,1);
sumRows = sumRows - min(sumRows);
sumRows = sumRows./max(sumRows);

outputSignal = sumRows;

% Test Plot
% lateralRange = linspace(startLateral, endLateral,length(sumRows));

% figure(4); plot(lateralRange,sumRows)


end

