% Title: DATA SCRAPING CODE - Thesis
% Author: Joel Sved 
% Date: 1/04/22

% Inputs: 1-D Signal (numTotalSamples x lateral steering px length) 
%                                                      (Dim ~= 200)                                                                      200)
%         2-D Image (numTotalSamples images) (Dim = 512 x 640)
% Outputs: Current vector (numTotalSamples x numChannels) (Dim = 8)

clear all
close all force
clc

%% PROMPT USER FOR INFO 

prompt1 = "Number of total samples: ";
totalSamples = input(prompt1);
prompt2 = "Number of samples per channel: ";
numSamples = input(prompt2);
prompt3 = "Max Current = sqrt[(20e-3)/R_channel]: ";
maxCurrent = input(prompt3);
prompt4 = "Start Lateral Px: ";
startLateral = input(prompt4);
prompt5 = "End Lateral Px: ";
endLateral = input(prompt5);
prompt6 = "Start Longitudinal Px: ";
startLongitude = input(prompt6);
prompt7 = "End Longitudinal Px: ";
endLongitude = input(prompt7);

%% INTIALISATION OF CAMERA ARDUINO

a1 = arduino('COM8', 'Uno', 'Libraries', 'I2C');
addr1 = scanI2CBus(a1,0);
addrs1 = scanI2CBus(a1);

camControlDev = i2cdev(a1,'0x60','bus',0);

% Open the images file
pathName = 'D:\WiDyVISION 3.7.0\Videos\';
fileName = 'test_joel.ptw';

fid = fopen(fileName,'r');

%% INTIALISATION OF CURRENT ARDUINO

% Arduino CURRENT SOURCE object Setup
a2 = arduino('COM9', 'Uno', 'Libraries', 'I2C');
addr2 = scanI2CBus(a2,0); % Scan the I2C bus to detect all the slaves adresses
initialization_DAC(a2); % Initialize all the DACs boards

% Here you can add a loop to on dev and n to update all DACs
curControlDev = i2cdev(a2,addr2{1},'bus',0); % Connect to the first DAC address

%% CREATION OF IMAGE FILES 
% Looking for the creation of the images file: start record on WidyVision
while fid==-1
    pause(0.1)
    fid = fopen([pathName fileName],'r');
    trigger(camControlDev)
end
 
% Attention: you only have five second to press the play button on
% WidyVision. If not, unplug the plug again the camera.
disp('start camera')
disp('4 secondes')
pause(1)
disp('3 secondes')
pause(1)
disp('2 secondes')
pause(1)
disp('1 secondes')
pause(1)

%% SET PSI & THETA AND INIT FRAME NUM
beamRes = 0.16;
psi = 253*beamRes; 
theta = 350 * beamRes;
targetAngle = [psi, theta];

frameNum = 0;

%% CURRENT SWEEP OVER 8 CHANNELS
numChannel = 8; 
minCurrent = 0; % mA
% maxCurrent = 3; % mA
% numSamples = 10; % Number of current samples per channel
currentIncrement = maxCurrent/numSamples; % Current increment between samples

%% FIRST CURRENT SWEEP
% Set current
currentZeros = zeros(1,numChannel);

%% SEND CURRENT AND FETCH RESULT
% Send current  
sendCurrentArduino(curControlDev, currentZeros, 1:numChannel)

% Fetch feedback from camera 
frameNum = frameNum +1;
originImage = newImage(camControlDev,fid, fileName, frameNum);

%% TRANSFORM INTO 1-D
originSignal = convert1D(originImage, startLateral, endLateral, ...
    startLongitude, endLongitude); %***


%% GIVEN NUMBER OF SAMPLES PER CHANNEL 'N', BUILD 'N' LAYERS OF 255 x 8 MATRIX

% Define Params
rows = (totalSamples-1); % Number of Different combinations (minus all 0's)
cols = numChannel;
completeMatrix = zeros(rows,cols,numSamples);
randomNum = zeros(1,rows*cols);

% Build next matrices
for i = 1:numSamples
    
    % Build array of random numbers, size(1,255*8)
    for j = 1:size(randomNum,2)
        randomCurrent = minCurrent + (maxCurrent-minCurrent)*rand();
        randomNum(j) =  randomCurrent; 
    end
    % reshape vector ==> (rows,cols)
    randomMatrix(:,:,i) = reshape(randomNum,[rows,cols]);
    
    completeMatrix(:,:,i) = ones(rows,cols) .* randomMatrix(:,:,i);

end

%% SAVE THESE APPLIED CURRENTS ==> .CSV FILE (OUTPUT)
out=reshape(permute(completeMatrix,[2 1 3]),size(completeMatrix,2),[])';
out = [currentZeros; out]; 
% first col is all 0 currents (origin)
% Rest is in 255 row blocks 
writematrix(out,'OUTPUT_8ch_current.csv');  

%% APPLY THESE CURRENTS AND GET RESULT 
for i = 1:size(out,1)
    sendCurrentArduino(curControlDev, out(i,:), 1:numChannel);
    disp('Applying Current');
    pause(0.3); % Wait for heat to distribute 
    frameNum = frameNum + 1;
    matrixImg(:,:,i) = newImage(camControlDev,fid, fileName, frameNum);
    % Perform 1-D operation 
    signal(i,:) = convert1D(matrixImg(:,:,i), startLateral, endLateral, ...
    startLongitude, endLongitude); %***
end

%% RESCALE & OUTPUT 1-D AND 2-D IMAGES TO .CSV
in2DImage=reshape(permute(matrixImg,[2 1 3]),size(matrixImg,2),[])';

% number of images = size(out,1) 
in2DImage = [orignImage;in2DImage]; 
writematrix(in2DImage,'INPUT_2D_image.csv');

% size of signal = size(out,1) x lateral pixel steering range
inSignal = [originSignal; signal]; 
writematrix(inSignal,'INPUT_1D_signal.csv'); 


