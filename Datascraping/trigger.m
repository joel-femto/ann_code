% Trigger function:
function trigger(dev)
 
tpause=0.05;
 
WriteChannelA=bin2dec('01011000'); % Select Single Write Command 01011 DacA 00 Update at the end 0
WriteChannelB=bin2dec('01011010'); % Select Single Write Command 01011 DacB 01 Update at the end 0
WriteChannelC=bin2dec('01011100'); % Select Single Write Command 01011 DacC 10 Update at the end 0
WriteChannelD=bin2dec('01011110'); % Select Single Write Command 01011 DacD 11 Update at the end 0
 
dataInSelect= bin2dec('0000000000000000'); % Vref VCC 1, PD1 PD0 normal mode 00, gain *1 0
register= WriteChannelA;  % Select channel to write
 
 
dataInValue=round(4095/5*3.3); %3.3 V
dataIn=dataInSelect+dataInValue;
writeRegister(dev,register,dataIn,'uint16')
pause(30e-3)
 
%pause(tpause)
dataInValue=round(0);
dataIn=dataInSelect+dataInValue;
writeRegister(dev,register,dataIn,'uint16')
pause(tpause)
 
end